package errd

import (
    "log"
    "fmt"
)


type Handler func (err error) error

// errptr is a type for a pointer to an error that has methods 
// desined on it for easy error handling.  
type errptr struct { 
    ptr *error
    handlers []Handler
}


// Closer is anything that implement a close method 
type Closer interface {
    Close() error
}

func New(err * error) *errptr {
    return &errptr{err, []Handler{}}
}

func (e *errptr) Apply(handler func (err error) error) *errptr {
    e.handlers = append(e.handlers, handler)
    return e
}

func (e * errptr) Do() *errptr {
    for _, handler := range e.handlers {
        if e.ptr != nil && *e.ptr != nil {
            *e.ptr = handler(*e.ptr)
        }
    }
    return e
}

func (e *errptr) Always(handler func (err error) error) *errptr {
    if e.ptr != nil {
        *e.ptr = handler(*e.ptr)
    }
    return e
}


func DoClose(close Closer) func (err error) error {
    return func (err error) error {
        closeErr := close.Close()       
        if closeErr != nil {
            return closeErr
        }
        return err
    }
}

func (e * errptr) Close(closer Closer) *errptr {
    return e.Apply(DoClose(closer))
}

func (e * errptr) AlwaysClose(closer Closer) *errptr {
    return e.Always(DoClose(closer))
}

func (e * errptr) Add(format string, args ... interface {}) *errptr {
    return e.Apply(func (err error) error {
        format = format + ": %v"
        args = append(args, err)
        return fmt.Errorf(format, args...)
    })
}

func (e * errptr) Wrap(format string, args ... interface {}) *errptr {
    return e.Apply(func (err error) error {
        format = format + ": %w"
        args = append(args, err)
        return fmt.Errorf(format, args...)
    })
}

func (e * errptr) Logf(format string, args ... interface {}) *errptr {
    return e.Apply(func (err error) error {
        format = format + ": %v"
        args = append(args, err)
        log.Printf(format, args...)
        return err
    })
}

func (e * errptr) Log() *errptr {
    return e.Logf("")
}

func (e * errptr) Printf(format string, args ... interface {}) *errptr {
    return e.Apply(func (err error) error {
        format = format + ": %v\n"
        args = append(args, err)
        fmt.Printf(format, args...)
        return err
    })
}

func (e * errptr) Print() *errptr {
    return e.Printf("")
}

func (e * errptr) Debug(extra string) *errptr {
    fmt.Printf("errptr Debug: %s: %v, %v\n", extra, e, *e.ptr)
    return e
}


func Apply(err * error, handler func (err error) error) *errptr {
    return New(err).Apply(handler).Do()
}

func Always(err * error, handler func (err error) error) *errptr {
    return New(err).Always(handler).Do()
}

func Add(err * error, format string, args ... interface {}) *errptr {
    return New(err).Add(format, args...).Do()
}

func Wrap(err * error, format string, args ... interface {}) *errptr {
    return New(err).Wrap(format, args...).Do()
}

func Logf(err * error, format string, args ... interface {}) *errptr {
    return New(err).Logf(format).Do()
}

func Log(err * error) *errptr {
    return New(err).Log().Do()
}

func Printf(err * error, format string, args ... interface {}) *errptr {
    return New(err).Printf(format, args...).Do()
}

func Print(err * error) *errptr {
    return New(err).Print().Do()
}

func Close(err * error, closer Closer) *errptr {
    return New(err).Close(closer).Do()
}

func AlwaysClose(err * error, closer Closer) *errptr {
    return New(err).AlwaysClose(closer).Do()
}



