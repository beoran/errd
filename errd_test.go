package errd

import (
    "testing"
    "fmt"
)


type closerMock struct {
    opened bool
    closed bool
}

func openMock() *closerMock {
    return &closerMock{true, false}
}

func badMock() *closerMock {
    return &closerMock{false, false}
}


func (c * closerMock) Close() (err error) {
    if !c.opened {
        return fmt.Errorf("closerMock not opened")
    }
    c.closed = true
    return nil
}

func causeError(doError bool, message string) error {
    if doError {
        return fmt.Errorf("error: %s", message)
    } else {
        return nil
    }
}


func tryAdd(doError bool) (err error) {
    defer New(&err).Add("tryAdd: %v", doError).Log().Do()
    return causeError(doError, "causeError TA") 
    return nil
}



func TestAdd1(t * testing.T) {
    err := tryAdd(false)
    if err != nil {
        t.Errorf("Err should be nil")
    }
}

func TestAdd2(t * testing.T) {
    err := tryAdd(true)
    if err == nil {
        t.Fatalf("Err should not be nil")    
    }
    expected := "tryAdd: true: error: causeError TA"
    if err.Error() != expected {
       t.Errorf("Expected: %s\nobserved: %s", expected, err.Error())
    }
}

func tryClose(closer Closer, doError bool) (err error) {
    defer New(&err).Close(closer).Log().Do()
    return causeError(doError, "causeError TC")
}

func tryCombined(closer Closer, doError bool) (err error) {
    defer New(&err).Add("closer %v", closer).Close(closer).Log().Do()
    return causeError(doError, "causeError CO")
}



func TestClose1(t * testing.T) {
    closer := openMock()
    err := tryClose(closer, false)
    if closer.closed {
        t.Errorf("Should not be closed")
    }    
    if err != nil {
        t.Errorf("Err should be nil")    
    }    
} 

func TestClose2(t * testing.T) {
    closer := badMock()
    err := tryClose(closer, true)
    if closer.closed {
        t.Errorf("Should not be closed")
    }    
    if err == nil {
        t.Errorf("Err should not be nil")
    }   
}

func TestClose3(t * testing.T) {    
    closer := openMock()
    err := tryClose(closer, true)
    if !closer.closed {
        t.Errorf("Should be closed")
    }
    if err == nil {
        t.Errorf("Err should not be nil")
    }
}

func TestCombined(t * testing.T) {    
    closer := openMock()
    err := tryCombined(closer, true)
    if !closer.closed {
        t.Errorf("Should be closed")
    }
    if err == nil {
        t.Errorf("Err should not be nil")
    }
}
